# Install_Open_AI_gym_on_Ubuntu16_with_anaconda
```{bash} 
$ conda info --envs
# conda environments:
#
python2.7                /usr/local/anaconda/anaconda2/envs/python2.7
python3                  /usr/local/anaconda/anaconda2/envs/python3
root                  *  /usr/local/anaconda/anaconda2

$ conda search python
Fetching package metadata .........
python                       3.5.3                         1  defaults

$ conda create --name python3.5 python=3.5.3
Fetching package metadata .........
Solving package specifications: .

Package plan for installation in environment /usr/local/anaconda/anaconda2/envs/python3.5:

The following NEW packages will be INSTALLED:

    openssl:    1.0.2l-0     
    pip:        9.0.1-py35_1 
    python:     3.5.3-1      
    readline:   6.2-2        
    setuptools: 27.2.0-py35_0
    sqlite:     3.13.0-0     
    tk:         8.5.18-0     
    wheel:      0.29.0-py35_0
    xz:         5.2.2-1      
    zlib:       1.2.8-3      

Proceed ([y]/n)? y

openssl-1.0.2l 100% |#################################################################| Time: 0:00:00  10.03 MB/s
python-3.5.3-1 100% |#################################################################| Time: 0:00:01  10.46 MB/s
setuptools-27. 100% |#################################################################| Time: 0:00:00   9.98 MB/s
wheel-0.29.0-p 100% |#################################################################| Time: 0:00:00  14.39 MB/s
pip-9.0.1-py35 100% |#################################################################| Time: 0:00:00  10.70 MB/s
#
# To activate this environment, use:
# > source activate python3.5
#
# To deactivate this environment, use:
# > source deactivate python3.5
#

$ source activate python3.5
(python3.5) $

(python3.5) $ conda info --envs
# conda environments:
#
python2.7                /usr/local/anaconda/anaconda2/envs/python2.7
python3                  /usr/local/anaconda/anaconda2/envs/python3
python3.5             *  /usr/local/anaconda/anaconda2/envs/python3.5
root                     /usr/local/anaconda/anaconda2


(python3.5) $ pip install gym
Collecting gym
  Downloading gym-0.9.2.tar.gz (157kB)
    100% |████████████████████████████████| 163kB 1.1MB/s 
Requirement already satisfied: numpy>=1.10.4 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from gym)
Requirement already satisfied: requests>=2.0 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from gym)
Requirement already satisfied: six in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from gym)
Requirement already satisfied: pyglet>=1.2.0 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from gym)
Requirement already satisfied: urllib3<1.23,>=1.21.1 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from requests>=2.0->gym)
Requirement already satisfied: idna<2.6,>=2.5 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from requests>=2.0->gym)
Requirement already satisfied: chardet<3.1.0,>=3.0.2 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from requests>=2.0->gym)
Requirement already satisfied: certifi>=2017.4.17 in /usr/local/anaconda/anaconda2/envs/python3.5/lib/python3.5/site-packages (from requests>=2.0->gym)
Building wheels for collected packages: gym
  Running setup.py bdist_wheel for gym ... done
  Stored in directory: /home/rwoo/.cache/pip/wheels/c9/7a/c2/c795ae5362b528f803147f0f250d7b890829e8865e516aabd9
Successfully built gym
Installing collected packages: gym
Successfully installed gym-0.9.2

(python3.5) $ conda install jupyter
Fetching package metadata .........
Solving package specifications: .

Package plan for installation in environment /usr/local/anaconda/anaconda2/envs/python3.5:

The following NEW packages will be INSTALLED:
...

(python3.5) $ jupyter notebook --notebook-dir=`pwd` --no-browser --ip=0.0.0.0
```
